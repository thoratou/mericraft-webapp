FROM httpd:2.4-alpine

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

COPY webapp/* /usr/local/apache2/htdocs/

EXPOSE 80
