SERVER_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))/webapp

.PHONY: all
all: run

.PHONY: run
run:
	@printf "\033[32m Run local mericraft-webapp\n\033[0m"
	@docker run -i --rm \
		-p 8000:80 \
		-v $(SERVER_ROOT):/usr/local/apache2/htdocs/ \
		httpd:2.4-alpine

.PHONY: run-prod
run-prod:
	@printf "\033[32m Run mericraft-webapp prod-like\n\033[0m"
	@docker build -t mericraft-webapp .
	@docker run --rm -i \
		-p 80:80 \
		mericraft-webapp
