import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import LoginPanel from './Login.js';
import './App.css';

function App() {
    return (
	<div className="container">
	  <Router>
	    <Switch>
	      <Route path="/">
	        <LoginPanel />
	      </Route>
	    </Switch>
          </Router>
	</div>
    );
}

export default App;
