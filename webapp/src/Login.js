import React from 'react';

const LoginCardHeader = () =>
    <div className="card-header">
      <h3>Connexion à Mericraft</h3>
      <div className="d-flex justify-content-end social_icon">
      <span><i className="fas fa-dice-d6"></i></span>
      </div>
    </div>;

class LoginSubmitForm extends React.Component {
    constructor(props) {
	super(props);
	this.state = {
	    email: '',
	    password: ''
	};

	this.handleEmailChange = this.handleEmailChange.bind(this);
	this.handlePasswordChange = this.handlePasswordChange.bind(this);
	this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleEmailChange(event) {
	this.setState({
	    email: event.target.value,
	    password: this.state.password
	});
    }

    handlePasswordChange(event) {
	this.setState({
	    email: this.state.email,
	    password: event.target.value
	});
    }

    handleSubmit(event) {
	alert('email: '+this.state.email+', password: '+this.state.password);
	event.preventDefault();
    }

    render() {
	return (
          <form id="loginForm" onSubmit={this.handleSubmit}>
            <div className="input-group form-group">
              <div className="input-group-prepend">
                <span className="input-group-text"><i className="fas fa-user"></i></span>
              </div>
	      <input id="email" type="text" className="form-control" placeholder="email" value={this.state.value} onChange={this.handleEmailChange} />
            </div>
            <div className="input-group form-group">
              <div className="input-group-prepend">
                <span className="input-group-text"><i className="fas fa-key"></i></span>
              </div>
	      <input id="password" type="password" className="form-control" placeholder="mot de passe" value={this.state.password} onChange={this.handlePasswordChange} />
            </div>
            <div className="form-group">
              <button id="loginBtn" type="submit" className="btn float-right login_btn">Se connecter</button>
            </div>
          </form>
	);
    }
}

const LoginCardBody = () =>
    <div className="card-body">
      <div id="errorMessage" className="alert alert-danger col-sm-12" ng-show="errorMessage"></div>
      <LoginSubmitForm/>
    </div>;
    

const LoginCardFooter = () =>
    <div className="card-footer">
      <div className="d-flex justify-content-center links">
        Pas encore de compte ? <a href="#signup" ui-sref="signup">S'inscrire</a>
      </div>
      <div className="d-flex justify-content-center">
        <a href="#forgetpassword">Mod de passe oublié ?</a>
      </div>
    </div>;

const LoginPanel = () =>
    <div id="loginPanel" className="d-flex justify-content-center h-100">
    <div className="card">
    <LoginCardHeader/>
    <LoginCardBody/>
    <LoginCardFooter/>
    </div>
    </div>;

export default LoginPanel;
